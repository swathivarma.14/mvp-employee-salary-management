import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Modal, Card, Collapse, Space, Upload, Message } from 'antd';
import './upload.less';
import { UploadOutlined } from '@ant-design/icons';
import EmployeeDetails from '../employeeDetails/employeeDetails';
import { CSVReader } from 'react-papaparse';
import Papa from 'papaparse';
import { UploadProps } from 'antd/lib/upload';

const UploadFile = (props) => {

    const [fileSizeErrMsg, setFileSizeErrMsg] = useState("");
    const [isCsvErrMsg, setIsCsvErrMsg] = useState("");
    const [csvError, setCsvError] = useState(false);
    const [sizeError, setSizeError] = useState(false);
    const [showDetails, setshowDetails] = useState(false);
    // State to store parsed data
    const [parsedData, setParsedData] = useState([]);
    //State to store table Column name
    const [tableRows, setTableRows] = useState([]);
    //State to store the values
    const [values, setValues] = useState([]);
    const [hideList, setHideList] = useState(false);
    const [uploadList, setUploadList] = useState([]);

    const handleChange = (info) => {
        console.log("info===", info);
        let newList = [...info.fileList];
        console.log("newList==", newList);
        
    };

    const beforeUpload = (file, fileList) => {
        const isCsv = file.type;
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            setSizeError(true);
            setFileSizeErrMsg("File size must be smaller than 2MB!");
            return false;
        } else if (isCsv !== "text/csv") {
            setCsvError(true);
            setIsCsvErrMsg("You can only upload .csv files")
            return false;
        } else {
            setUploadList(uploadList.concat(fileList));
        }

        Papa.parse(file, {
            download: true,
            header: true,
            complete: data => {
              setValues(data);
              setshowDetails(true);
              setHideList(true);
              setFileSizeErrMsg("");
              setIsCsvErrMsg("")
            }
          });
        return false;
    }

    const onRemove = (file) => {
        const filteredFiles = uploadList.filter((fileObj) => {
            return (fileObj.uid !== file.uid)
        });
        setUploadList(filteredFiles);
        setSizeError(false);
    }

    useEffect(() => {
      }, [isCsvErrMsg, fileSizeErrMsg, csvError]);

    return (
        <>
            <Row style={{margin: "20px"}}>
                <Space direction="vertical" style={{ width: '100%' }} size="large">
                    {sizeError && <span style={{ color: "red" }}>{fileSizeErrMsg}</span>}
                    {csvError && <span style={{ color: "red" }}>{isCsvErrMsg}</span>}
                    <Upload
                        listType="picture"
                        className="uploader"
                        beforeUpload={beforeUpload}
                        onRemove={onRemove}
                        accept="text/csv"
                        showUploadList={hideList}
                        onChange={handleChange}
                        fileList={uploadList}
                        maxCount={1}
                    >   
                        <Button type="danger" icon={<UploadOutlined />}>Click Upload</Button>
                    </Upload>
                    <span style={{ color: "grey" }}>Note: You can only upload csv files!</span>
                </Space>
            </Row>
            {showDetails && <EmployeeDetails tableRows={tableRows} values={values.data}/>}
        </>
    )
}

export default UploadFile;