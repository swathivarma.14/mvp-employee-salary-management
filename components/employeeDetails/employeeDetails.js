import React, { useState, useEffect, useRef } from 'react';
import { Row, Col, Button, Space, Table, Input, Modal } from 'antd';
import './employeeDetails.less';
import data from "./data.json";
import { SearchOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import Highlighter from 'react-highlight-words';

const { Search } = Input;

const EmployeeDetails = (props) => {

    const { values} = props;

  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const searchInput = useRef(null);
  const [minValue, setMinValue] = useState("");
  const [maxValue, setMaxValue] = useState("");
  const [dataSource, setDataSource] = useState([]);
  const [showEditModal, setShowEditModal] = useState(false);
  const [editingEmp, setEditingEmp] = useState(null);
  const [minErrorMsg, setMinErrorMsg] = useState("");
  const [maxErrorMsg, setMaxErrorMsg] = useState("");

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText('');
  };

  useEffect(() => {
    const data = values.filter(obj => Object.values(obj).every(val => val !== ''));
    setDataSource(data);
  }, [values]);

  const handleOk = () => {
    setShowEditModal(false);
  };

  const handleCancel = () => {
    setShowEditModal(false);
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div
        style={{
          padding: 8,
        }}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 90,
            }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({
                closeDropdown: false,
              });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? '#1890ff' : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: '#ffc069',
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      width: '30%',
      ...getColumnSearchProps('id'),
    },
    {
      title: 'Login',
      dataIndex: 'login',
      key: 'login',
      width: '20%',
      ...getColumnSearchProps('login'),
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      ...getColumnSearchProps('name'),
      sorter: (a, b) => a.name.localeCompare(b.name),
      sortDirections: ['descend', 'ascend'],
    },
    {
        title: 'Salary',
        dataIndex: 'salary',
        key: 'salary',
        ...getColumnSearchProps('salary'),
        sorter: (a, b) => a.salary - b.salary,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Action',
        dataIndex: 'salary',
        key: 'action',
        render: (_, record) => (
            <Space size="middle">
              <Button
                type="text"
                style={{ padding: "0px", color: "#062077" }}
                onClick={() => {
                    onEdit(record)
                }}
              >
                <EditOutlined />
              </Button>
              <Button
                type="text"
                style={{ color: "#062077" }}
                onClick={() => {
                    onDelete(record)
                }}
              >
                <DeleteOutlined />
              </Button>
            </Space>
          )
      }
  ];

  const onSearch = () => {
    if (minValue !== "" && maxValue !== "") {
      const data = values.filter(obj => Object.values(obj).every(val => val !== ''));
      const filteredData = data.filter((emp) => {
        return (parseInt(emp.salary) > parseInt(minValue) && parseInt(emp.salary) < parseInt(maxValue))
      });
      setMinErrorMsg("");
      setMaxErrorMsg("");
      setDataSource(filteredData);
    }
    if (minValue === "") {
      setMinErrorMsg("Minimum salary field cannot be empty");
    }
    if (maxValue === "") {
      setMaxErrorMsg("Maximum salary field cannot be empty");
    }
  };

  const onReset = () => {
    setMinValue("");
    setMaxValue("");
    const data = values.filter(obj => Object.values(obj).every(val => val !== ''));
    setDataSource(data);
  };

  const onMinInputChange = (e) => {
    const inputValue = e.target.value;
    setMinValue(inputValue);
  };

  const onMaxInputChange = (e) => {
    const inputValue = e.target.value;
    setMaxValue(inputValue);
  };

  const onDelete = (record) => {
    Modal.confirm({
        title: "Are you sure you want to delete this employee record ?",
        okText: "Yes",
        okType: "danger",
        onOk: () => {
            setDataSource(pre => {
                return pre.filter(emp => emp.id !== record.id);
            })
        }
    })
  }

  const onEdit = (record) => {
    setShowEditModal(true);
    setEditingEmp({...record});
  }

  const onSave = () => {
    setShowEditModal(false);
    setEditingEmp(null);
    setDataSource(pre => {
        return pre.map(emp => {
            if(emp.id === editingEmp.id) {
                return editingEmp;
            } else {
                return emp;
            }
        })
    })
  }

  return (
    <>
        <Row style={{ display: "flex", marginBottom: "50px" }}>
            <Col style={{ paddingLeft: "20px" }}>
                <Input
                    placeholder="Minimum salary"
                    value={minValue || ""}
                    type="text"
                    onChange={onMinInputChange}
                />
                <span style={{ color: "red" }}>{minErrorMsg}</span>
            </Col>
            <Col style={{ paddingLeft: "20px" }}>
                <Input
                    placeholder="Maximum Salary"
                    value={maxValue || ""}
                    type="text"
                    onChange={onMaxInputChange}
                />
                <span style={{ color: "red" }}>{maxErrorMsg}</span>
            </Col>
            <Col style={{ paddingLeft: "20px" }}>
                <Button type="danger" onClick={onSearch}>Search</Button>
            </Col>
            <Col style={{ paddingLeft: "20px" }}>
                <Button type="danger" onClick={onReset}>Reset</Button>
            </Col>
        </Row>
        <Table
            columns={columns}
            dataSource={dataSource}
            rowClassName = {(record, index) => index % 2 === 0 ? 'table-row-light' :  'table-row-dark'}
            pagination={{
            defaultPageSize: 5,
            pageSize: "5",
        }}/>
        <Modal title="Edit" visible={showEditModal} footer={null} onCancel={handleCancel}>
            <h3>Employee Id: {editingEmp?.id}</h3>
            <Row style={{ marginBottom: "20px"}}>
                <label>Login</label>
                <Input
                    style={{ borderRadius: "5px" }}
                    value={editingEmp?.login}
                    onChange={(e) => {
                        setEditingEmp(pre => {
                            return {...pre, login: e.target.value}
                        })
                    }}
                />
            </Row>
            <Row style={{ marginBottom: "20px"}}>
                <label>Name</label>
                <Input
                    style={{ borderRadius: "5px" }}
                    value={editingEmp?.name}
                    onChange={(e) => {
                        setEditingEmp(pre => {
                            return {...pre, name: e.target.value}
                        })
                    }}
                />
            </Row>
            <Row style={{ marginBottom: "20px"}}>
                <label>Salary</label>
                <Input
                    style={{ borderRadius: "5px" }}
                    value={editingEmp?.salary}
                    onChange={(e) => {
                        setEditingEmp(pre => {
                            return {...pre, salary: e.target.value}
                        })
                    }}
                />
            </Row>
            <Row style={{ marginBottom: "20px"}}>
                <Button type="primary" onClick={onSave}>Save</Button>
            </Row>
        </Modal>
    </>
  );
};

export default EmployeeDetails;