/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from 'react';
import { Row, Col, Image } from 'antd';
import styles from '../../styles/sidebar.module.css';
import { UploadOutlined } from '@ant-design/icons';

const Sidebar = (props) => {

    const imageProps = {
        src: '/assets/png/user.png',
        alt: 'logo',
        width: 50,
        height: 50,
    };

    const fileImageProps = {
      src: '/assets/png/file.png',
      alt: 'func',
      width: 20,
      height: 20,
    };

    return (
        <div className='sidebar'>
            <div>
                <Row className={styles.image}>
                    <Image {...imageProps}/>
                </Row>
            </div>
            <Row className={styles.userName}>User Name</Row>
            <div className="function">
                
                <Row className={styles.funcRow}>
                <Col><Image {...fileImageProps}/></Col>
                <Col><p>Function 1</p></Col>
                </Row>
                <Row className={styles.funcRow}>
                <Col><Image {...fileImageProps}/></Col>
                <Col><p>Function 2</p></Col>
                </Row>
                <Row className={styles.funcRow}>
                <Col><Image {...fileImageProps}/></Col>
                <Col><p>Function 3</p></Col>
                </Row>
                <Row className={styles.funcRow}>
                <Col><Image {...fileImageProps}/></Col>
                <Col><p>Function 4</p></Col>
                </Row>
                <Row className={styles.funcRow}>
                <Col><Image {...fileImageProps}/></Col>
                <Col><p>Function 5</p></Col>
                </Row>
            </div>
        </div>
    )
}

export default Sidebar;