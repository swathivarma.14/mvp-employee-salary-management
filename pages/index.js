/* eslint-disable jsx-a11y/alt-text */
import Head from 'next/head'
import UploadFile from '../components/upload/upload'
import styles from '../styles/Home.module.css'
import { Layout, Menu, Row, Col, Image, Typography  } from 'antd';
import { UploadOutlined, UserOutlined } from '@ant-design/icons';
import Sidebar from '../components/sidebar/sidebar';

const { Header, Footer, Sider, Content } = Layout;
const { Title } = Typography;

export default function Home() {
  return (
    // <div className={styles.container}>

    //   <Head>
    //       <title>Place Order</title>
    //   </Head>
    //   <Layout hasSider={true}>
    //     <Sider className={styles.layoutSider} trigger={null} collapsible>
    //         <Sidebar />
    //     </Sider>
    //     <Layout className={styles.layout}>
    //       <div className={styles.layoutRightSection}>
    //         <div className={styles.layoutPadding}>
    //           <UploadFile />
    //         </div>
    //       </div>  
    //     </Layout>
    //   </Layout>    
    // </div>

    <div className={styles.app}>
      <Layout>
      <Header><Title level={2} className={styles.header}>MVP Employee Details</Title></Header>
      <Layout>
        <Sider className={styles.sider}>
          <Sidebar />
        </Sider>
        <Layout className={styles.content}>
          <Content>
            <UploadFile />
          </Content>
          <Footer>Footer</Footer>
        </Layout>
      </Layout>
    </Layout>
    </div>
  )
}
