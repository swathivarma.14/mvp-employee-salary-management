import '../styles/globals.css';
import "../styles/antd.less"; // Add this line 

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
